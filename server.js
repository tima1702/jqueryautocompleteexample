const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();

app.use(express.static('static'));

function readJson(path, cb) {
  fs.readFile(require.resolve(path), function (err, data) {
    if (err)
      cb(err);
    else
      cb(null, JSON.parse(data));
  });
}

app.get('/search', function (req, res) {
  if (!req.query.q) {
    res.status(400).send({
      message: 'Query is required',
    });

    return;
  }

  const reg = new RegExp(req.query.q, 'ig');
  readJson('./countries.json', function (err, data) {
    res.json(data.filter(({ name }) => name.match(reg)));
  });
});

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + 'static/index.html'));
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});